package root.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import root.entity.Weather;

import java.util.List;

@Repository
public interface ICanWorkWithDB extends CrudRepository<Weather, Integer> {

    Weather save(Weather weather);

    List<Weather> findAll();

}
