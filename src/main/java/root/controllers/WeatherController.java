package root.controllers;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import root.dao.ICanWorkWithDB;
import root.db.WeatherDao;
import root.entity.Weather;

import java.util.List;

@Controller
@RequestMapping("/weather")
@AllArgsConstructor
public class WeatherController {

    private ICanWorkWithDB iCanWorkWithDB;

    @GetMapping
    public String helloWorldController(Model model) {
        List<Weather> initialList = new WeatherDao(iCanWorkWithDB).findAll();

        if (initialList.size() == 0) {
            return "weather";
        }

        int toIndex = initialList.size();
        int fromIndex = initialList.size() >= 9 ? toIndex - 8 : 0;

        model.addAttribute("weather", initialList.subList(fromIndex, toIndex));
        return "weather";
    }


}