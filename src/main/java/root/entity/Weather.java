package root.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "weather")
public class Weather {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "City")
    private String name;
    @Column
    private String temperature;
    @Column
    private String feelsLike;
    @Column
    private String pressure;
    @Column
    private String humidity;
    @Column
    private String serviceType;
}
