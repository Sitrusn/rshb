package root.helpers;

import org.json.JSONObject;

public class WSParsingHelper extends JSONObject {

    public WSParsingHelper.location location;
    public WSParsingHelper.current current;

    public class location {
        public String name;
    }

    public class current {
        public String temperature;
        public String pressure;
        public String humidity;
        public String feelslike;
    }

}
