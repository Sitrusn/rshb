package root.parser;

import com.google.gson.Gson;
import org.springframework.stereotype.Component;
import root.entity.Weather;
import root.gateway.WeatherTaker;
import root.helpers.OWMParsingHelper;
import root.helpers.WSParsingHelper;

import java.util.List;

@Component
public class Parser {

    public Weather getUniformWeather() {

        List<String> weather = new WeatherTaker().getWeather();
        Weather entity = new Weather();

        switch (weather.get(0)) {
            case "owmRequest":
                OWMParsingHelper mapModel = new Gson()
                        .fromJson(weather.get(1), OWMParsingHelper.class);

                entity.setName(mapModel.name);
                entity.setTemperature(mapModel.main.temp);
                entity.setFeelsLike(mapModel.main.feels_like);
                entity.setPressure(mapModel.main.pressure);
                entity.setHumidity(mapModel.main.humidity);
                entity.setServiceType("OpenWeatherMap");
                break;

            case "wsRequest":
                WSParsingHelper stackModel = new Gson()
                        .fromJson(weather.get(1), WSParsingHelper.class);

                if (stackModel.isEmpty()) {
                    entity.setName("-");
                    entity.setHumidity("-");
                    entity.setFeelsLike("-");
                    entity.setPressure("-");
                    entity.setTemperature("-");
                    entity.setServiceType("WeatherStock");
                } else {
                    entity.setName(stackModel.location.name);
                    entity.setHumidity(stackModel.current.humidity);
                    entity.setFeelsLike(stackModel.current.feelslike);
                    entity.setPressure(stackModel.current.pressure);
                    entity.setTemperature(stackModel.current.temperature);
                    entity.setServiceType("WeatherStock");
                }
                break;
        }

        return entity;
    }
}