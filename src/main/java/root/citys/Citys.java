package root.citys;

public enum Citys {
    Moscow,
    Izhevsk,
    Rome,
    Ufa,
    Kazan,
    London;

    public static Citys getRandomCity() {
        return values()[(int) (Math.random() * values().length)];
    }
}
