package root.gateway;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import root.citys.Citys;

import java.util.List;
import java.util.Random;

@Component
public class WeatherTaker {

    public List<String> getWeather() {
        String cityName = Citys.getRandomCity().name();

        String owmRequest = "https://api.openweathermap.org/data/2.5/weather?units=metric&appid=3076f736fb3d1da04f92ee12443ddb37&q=" + cityName;
        String wsRequest = "http://api.weatherstack.com/current?access_key=abb955b8537e5a202c9ee554d17d59bb&units=m&query=" + cityName;

        List<String> requestList = List.of(owmRequest, wsRequest);
        int requestAttribute = new Random().nextInt(requestList.size());
        String randomRequest = requestList.get(requestAttribute);
        String serverAnswer = new RestTemplate().getForEntity(randomRequest, String.class).getBody();
// will add some notes, thought...
        return List.of(requestAttribute == 0 ? "owmRequest" : "wsRequest", serverAnswer);
    }

}
