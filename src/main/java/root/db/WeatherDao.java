package root.db;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import root.dao.ICanWorkWithDB;
import root.entity.Weather;
import root.parser.Parser;

import java.util.List;

@Component
public class WeatherDao {

    private ICanWorkWithDB iCanWorkWithDB;

    @Autowired
    private Parser parser;

    @Autowired
    public WeatherDao(ICanWorkWithDB iCanWorkWithDB) {
        this.iCanWorkWithDB = iCanWorkWithDB;
    }

    @Scheduled(initialDelay = 0, fixedDelay = 5 * 1000)
    public void save() {
        iCanWorkWithDB.save(parser.getUniformWeather());
        iCanWorkWithDB.save(parser.getUniformWeather());
        iCanWorkWithDB.save(parser.getUniformWeather());
    }

    public List<Weather> findAll(){
        return iCanWorkWithDB.findAll();
    }

}
